<!doctype html>
<html lang='pt-BR'>
<head>

    <?php 
        if ( $_SERVER['HTTP_HOST'] == 'localhost' )  {
                    
            $base = 'http://localhost/fernandomalta/';
            
        } else {
            
            $base = '/';

        }

     ?>

    <meta charset='UTF-8'>

    <meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=no'>

    <title>Fernando Malta Advogados</title>

    <base href="<?= $base; ?>">

    <!-- Favicon -->
    <link rel='icon' href='img/favicon.png'>

    <!-- Css -->
    <link rel='stylesheet' href='css/main.css'>
    <link rel='stylesheet' href='css/fonts.css'>
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <!-- Analytics
    <script src='js/analytics.js'></script>
     -->

    <!-- HTML5 Shiv -->
    <!--[if lt IE 9]><script src='js/html5.js'></script><![endif]-->

</head>
<?php
    extract($_GET);
?>
<body class="<?= $page; ?>">
    
    <?php 
        require 'pages/header.php';
        require 'pages/'.$page.'.php';
        require 'pages/footer.php';
    ?>


    <!-- Js -->
    <script src='js/jquery.js'></script>
    <script src='js/input-validate.js'></script>
    <script src='js/send-form.js'></script>
    <script src='js/slider.js'></script>
    <!-- <script src='js/unbounce.js'></script> -->
    <!-- <script src='js/scroll.js'></script> -->
    <script src='js/swipe.js'></script>
    <script src='js/responsive-nav.js'></script>

    <script>
        
    </script>

    
</body>
</html>