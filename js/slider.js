gallery              = document.querySelectorAll('#gallery figure img');
gallery_arrows       = document.querySelectorAll('#gallery .ctrl');
//gallery_thumbs       = document.querySelectorAll('#gallery .thumbs button');


// change gallery on thumbs
for ( var i = 0 ; i < gallery.length ; i++ ) {

    gallery_thumbs[i].onclick = function() {

        alert()

        n = this.getAttribute('data-number') ;

        for ( var i = 0 ; i < gallery_thumbs.length ; i++ ) {
            gallery[i].classList.remove('active');
            gallery_thumbs[i].classList.remove('active');
        }
          
        this.classList.add('active');
        gallery[n].classList.add('active');

    }
}

// change gallery on arrows
for ( var i = 0 ; i < gallery_arrows.length ; i++ ) {

    gallery_arrows[i].onclick = function() {

        var n = document.querySelector('#gallery img.active').getAttribute('data-number');

        switch(n) {
            case '1' : n = 1; break;
            case '4' : n = 0; break;
        }

        for ( var i = 0 ; i < gallery_thumbs.length ; i++ ) {
            gallery[i].classList.remove('active');
            gallery_thumbs[i].classList.remove('active');
        }

        gallery[n].classList.add('active');
        gallery_thumbs[n].classList.add('active');

    }
}