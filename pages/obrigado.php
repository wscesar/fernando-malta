<style>




</style>

<section id="banner" >
  <div class="wrap">
    <ul>
      <li>Mensagem</li>
      <li>Enviada com</li>
      <li>Sucesso</li>
    </ul>
    
  </div>
</section>

<section id="atuacao">
  <div class="wrap">

    <p class="title">Áreas de Atuação</p>
    <p class="subtitle">Fernando Malta Sociedade de Advogados atua em diversas áreas do direito</p>

    <div class="icons flex">

      <a href="direito-previdenciario">
        <img src="img/icon-document.png" alt="">
        <span>Direito Previdenciário</span>
      </a>

      <a href="direito-de-familia">
        <img src="img/icon-family.png" alt="">
        <span>Direito de Família</span>
      </a>

      <a href="direito-trabalhista">
        <img src="img/icon-briefcase.png" alt="">
        <span>Direito Trabalhista</span>
      </a>

      <a href="direito-civil">
        <img src="img/icon-hammer.png" alt="">
        <span>Direito Civil</span>
      </a>

    </div>
  </div>
</section>


<section class="pre-footer">
  <div class="wrap">
    <p class="title">Fernando Malta Sociedade de Advogados</p>
    <p class="subtitle">Composta por profissionais altamente qualificados e preparados.</p>

    <div class="content">
      
      <img src="img/icon-money.png" >
      <p>prezamos pela exceêncioa técnica e profissional</p>

      <img src="img/icon-people.png" >

      <p>alta capacidade na assistência a clientes de qualquer setor econômico</p>
    </div>
  </div>
</section>


