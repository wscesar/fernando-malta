<section class="inner">
  <div class="wrap">

      <p class="title">Direito Trabalhista</p>

      <img src="img/icon-briefcase.png" alt="">
      
      <p>Atua na área trabalhista propondo ações em favor do empregado ou representando empregadores, bem como na área consultiva e preventiva, além de militar em todo tipo de causa e reclamação trabalhista envolvendo relação jurídica existente entre empregador e empregado, como acidente de trabalho e doenças ocupacionais, danos morais e materiais, rescisões indiretas de contrato de trabalho, dentre outros. O escritório atua também em defesa de ações trabalhistas e orientações para prevenções de demandas judiciais e empresariais etc.</p>

    </div>
  
</section>


<?php require 'pages/any-question.php' ?>