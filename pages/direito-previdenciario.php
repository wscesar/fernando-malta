<section class="inner">
  <div class="wrap">

      <p class="title">Direito Previdenciário</p>

      <img src="img/icon-document.png" alt="">
      
      <p>Representando interesses de segurados junta previdência social em busca de direitos como aposentadorias por tempo de contribuição, especial, por idade, rural, pensão por morte, revisões de benefícios, restabelecimento de auxílio doença, auxílio acidente, salário-maternidade, benefício assistencial, entre outras situações.</p>

    </div>
  
</section>

<?php require 'pages/any-question.php' ?>