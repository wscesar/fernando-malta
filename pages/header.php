<header>
  
  <div class="wrap">
    <a href="<?= $base; ?>">
      <img class="logo" src="img/logo.png"/>
    </a>
    
    <button><span class="stripe"></span><span class="stripe"></span><span class="stripe"></span></button>


    
    <nav>
      <a class="<?php if ($page == 'escritorio') echo 'active' ; ?>" href="escritorio">Escritório</a>
      <a class="<?php if ($page == 'direito-previdenciario') echo 'active' ; ?>" href="direito-previdenciario">Direito Previdenciário</a>
      <a class="<?php if ($page == 'direito-de-familia') echo 'active' ; ?>" href="direito-de-familia">Direito de Família</a>
      <a class="<?php if ($page == 'direito-trabalhista') echo 'active' ; ?>" href="direito-trabalhista">Direito Trabalhista</a>
      <a class="<?php if ($page == 'direito-civil') echo 'active' ; ?>" href="direito-civil">Direito Civil</a>
      <a class="<?php if ($page == 'contato') echo 'active' ; ?>" href="contato">Contato</a>
    </nav>

  </div>

</header>

<script>
  window.onscroll = function(){
    
    window_top_position = window.pageYOffset;
    
    header = document.querySelector('header');
    header_height = document.querySelector('header').offsetHeight;

      if (window_top_position > 0) {

          header.classList.add('fixed');


      }else{
          header.classList.remove('fixed')

      }

  }
</script>