<div class="wrap">

  <?php require 'pages/form.php' ?>

</div>

<div class="googlemap">
  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3666.8867556556384!2d-46.872268185028766!3d-23.21079728485803!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94cf212ced5a0b7d%3A0xe6855c4186454732!2sR.+Santa+Catarina%2C+767+-+Jardim+Esplanada%2C+Jundia%C3%AD+-+SP%2C+13202-150!5e0!3m2!1spt-BR!2sbr!4v1555420927327!5m2!1spt-BR!2sbr" width="200" height="400" frameborder="0" allowfullscreen></iframe>

  <div class="data">
    
    <div class="address">
      <img src="img/icon-map.png" alt="">
      <span class="title">Endereço</span>
      <span>Rua Santa Catarina, 767</span>
      <span>Jardim Esplanada</span>
      <span>Jundiaí - SP</span>
      <span>CEP: 13202-150</span>
    </div>

    <div class="email">
      <img src="img/icon-letter.png" alt="">
      <span class="title">Email</span>
      <a>contato@fernandomalta.adv.br</a>
    </div>

    <div class="phone">
      <img src="img/icon-phone.png" alt="">
      <span class="title">Contatos</span>
      <a href="tel:11 4816 4323">(11) 4816-4323</a>
      <a href="tel:11 2709 2365">(11) 2709-2365</a>
      <a href="tel:11 2816 2370">(11) 2816-2370</a>
    </div>

  </div>

</div>