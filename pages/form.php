<form class="flex" data-form="#contact_form" onsubmit="send_form(this); return false" id="contact_form">
  
    <p class="title">
    	Envie sua Mensagem
    </p>

    <div class="textbox">
      <input type="text" id="name" name="name" required="required" class="text name"/>
      <label for="name">Nome</label>
    </div>

    <div class="textbox">
      <input type="email" id="email" name="email" data-form="#contact_form" required="required" class="text email"/>
      <label for="email">Email</label>
    </div>

    <div class="textbox">
      <input type="tel" id="subject" name="subject" class="text subject"/>
      <label for="subject">Assunto</label>
    </div>
    
    <div class="textbox">
      <textarea id="msg" name="msg" class="text msg"></textarea>
      <label for="msg">Mensagem</label>
    </div>

    <div class="textbox">
      <button class="submit">enviar</button>
  </div>
  
</form>