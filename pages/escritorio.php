<section class="inner">
  <div class="wrap flex">

  	<div class="text">
  		
      <p class="title">O Escritório</p>

      <p>O escritório FERNANDO MALTA SOCIEDADE DE ADVOGADOS possui estrutura para atender os interesses de seus clientes, com sede própria e de fácil localização conta com advogados capacitados e uma equipe de profissionais altamente qualificados. Atuando com ética e responsabilidade, além de contar com profissionais de alta qualidade.</p>
      <p>Em decorrência de sua atuação diversificada e da excelência de seus profissionais, o escritório é capaz de prestar assistência a clientes de qualquer setor econômico em todas as áreas do Direito, o que lhe permite desenvolver relacionamentos de confiança mútua, entre Escritório e cliente.</p>

  	</div>

	  	<div class="flex gallery">	

	  		<figure>
	  			<img
	  				data-number="0" 
	  				src="img/fernando-malta-office-01.jpg" alt=""
  				>
	  		</figure>

	  		<figure>
	  			<img
	  				data-number="1" 
	  				src="img/fernando-malta-office-02.jpg" alt=""
  				>
	  		</figure>

	  		<figure>
	  			<img
	  				data-number="2" 
	  				src="img/fernando-malta-office-03.jpg" alt=""
  				>
	  		</figure>

	  		<figure>
	  			<img
	  				data-number="3" 
	  				src="img/fernando-malta-office-04.jpg" alt=""
  				>
	  		</figure>


	  	</div>

    </div>
  
</section>

<section class="employers">	
	<div class="wrap">	
		<div class="flex fcol">
			<p>	O escritório está sob o comando do Dr. Fernando Malta, advogado desde 2006, referência no meio jurídico, atuando em todas as áreas do direito, se destaca devido ao excelente trabalho que desempenha na defesa dos interesses de seus clientes, com vasta experiencia processual, extremamente profissional, zelando pela ética e lealdade processual..... atuando de forma ética, vem ganhando espaço</p>
			
			<div class="pictures flex frow">
				
				<figure>
					<img src="img/fernando.png" alt="">
					<figcaption>
						<span>Dr.</span>
						Fernando Malta
					</figcaption>
				</figure>

				<figure>
					<img src="img/secretaria.jpg" alt="">
					<figcaption>
						<span>Dr<sup>a</sup></span>
						Andreia Schoiser Pereira Agostinho
					</figcaption>
				</figure>

				<figure>
					<img src="img/estagiario.jpg" alt="">
					<figcaption>
						<span>Estagiário</span>
						Leandro de Souza Dota
					</figcaption>
				</figure>

				<figure>
					<img src="img/secretaria.jpg" alt="">
					<figcaption>
						<span>Secretária</span>
						Nereira Bellinato
					</figcaption>
				</figure>

			</div>

			<p>Nosso time tem forte viés consultivo e preventivo, com foco nos aspectos técnicos e legais aplicáveis à realidade de cada cliente. Além disso, possui experiência em investigações administrativas, negociações com autoridades e contencioso judicial neste tema delicado, relevante e complexo.</p>
		</div>
	</div>
</section>


<?php require 'pages/any-question.php' ?>



<style>
	.slider{
		position: fixed;
		width: 100%;
		height: 100%;
		background: #0009;
		z-index: 999;
		display: none;
	}

	.slider.active{
		display: block
	}

	.slider img{
		max-height: 80%;
		position: absolute;
		left: 50%;
		top: 50%;
		transform: translateX(-50%) translateY(-50%) ;
		opacity: 0;
	}

	.slider img.active{
		opacity: 1;
	}
	
	.slider .ctrl{
		position: absolute;
		top: 50%;
		transform: translateY(-50%);
		height: 20px;
		color: #0f0;
		font-size: 36px;
		width: 5px;
		display: none;
	}

	.slider .ctrl.fa-arrow-left{
		left: 50px;
	}

	.slider .ctrl.fa-arrow-right{
		right: 50px;
	}
	.slider .close{
		font-size: 72px;
		color: #fff;
		right: 30px;
		top: 15px;
		position: absolute;
		cursor: pointer;
	}

	.slider .close:hover{
		color: #bd1515;

	}


</style>

<div class="slider">

	<span class="close">&times;</span>
	<span class="ctrl fa-arrow-left"></span>
	<span class="ctrl fa-arrow-right"></span>

	<img data-number="0"  src="img/fernando-malta-office-01.jpg" alt="">
	<img data-number="1"  src="img/fernando-malta-office-02.jpg" alt="">
	<img data-number="2"  src="img/fernando-malta-office-03.jpg" alt="">
	<img data-number="3"  src="img/fernando-malta-office-04.jpg" alt="">
	
		
</div>



<script>
	slider  = document.querySelector('.slider');
	close  = document.querySelector('.slider .close');
	images  = document.querySelectorAll('.slider img');
	thumbs = document.querySelectorAll('.gallery figure img');
	
	arrows = document.querySelectorAll('.gallery .ctrl');
	arrow_left = document.querySelectorAll('.gallery .ctrl.fa-arrow-left');
	arrow_right = document.querySelectorAll('.gallery .ctrl.fa-arrow-right');

	thumbs.forEach(element => {
	    
	    element.onclick = function() {

	        n = this.getAttribute('data-number');
	        slider.classList.add('active');
			images.forEach(el => {
				el.classList.remove('active');
			});
			images[n].classList.add('active');

	    }

	});

	close.onclick = function(){
		slider.classList.remove('active')
	}

	slider.onclick = function(){
		slider.classList.remove('active')
	}


/*
	arrows.forEach(element => {
		element.onclick = function() {
			var n = document.querySelector('.gallery img.active').getAttribute('data-number');
			alert(n)
		}
	})
*/

/*
	for ( var i = 0 ; i < arrows.length ; i++ ) {

	    alert(i)
	    arrows[i].onclick = function() {

	        var n = document.querySelector('.gallery img.active').getAttribute('data-number');

	        switch(n) {
	            case '1' : n = 1; break;
	            case '4' : n = 0; break;
	        }

	        for ( var i = 0 ; i < thumbs.length ; i++ ) {
	            images[i].classList.remove('active');
	            thumbs[i].classList.remove('active');
	        }

	        images[n].classList.add('active');
	        thumbs[n].classList.add('active');

	    }
	}
*/







</script>