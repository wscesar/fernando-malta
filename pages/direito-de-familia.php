<section class="inner">
  <div class="wrap">

      <p class="title">Direito Previdenciário</p>

      <img src="img/icon-family.png" alt="">
      
      <p>Atua de forma ampla no direito de família, tais como inventário, herança, testamento, separação, divórcio, pensão alimentícia, tutela, guarda, adoção, curatela e reconhecimento de paternidade. Além de desenvolvimento de contratos, nas situações de partilha de bens, nas ações de reconhecimento e de dissolução de união estável, união homo afetiva, assim como a representação preventiva, como pacto pré-nupcial, contrato de convivência e escrituras de doação. Retificação de registro civil etc.</p>

    </div>
  
</section>

<?php require 'pages/any-question.php' ?>